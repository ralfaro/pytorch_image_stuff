import cv2
import torch
from torchvision.transforms import ToTensor, ToPILImage
from PIL import Image
import numpy as np

# Función para mejorar los detalles de la imagen utilizando OpenCV
def enhance_details(image):
    # Convertir la imagen a escala de grises
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Aplicar un filtro de alta frecuencia para mejorar los detalles
    high_freq = cv2.Laplacian(gray, cv2.CV_64F)
    # Normalizar y convertir de nuevo a formato de imagen
    high_freq = cv2.normalize(high_freq, None, 0, 255, cv2.NORM_MINMAX)
    high_freq = np.uint8(high_freq)
    # Convertir de nuevo a una imagen en color
    enhanced_image = cv2.cvtColor(high_freq, cv2.COLOR_GRAY2BGR)
    return enhanced_image

# Función para realizar el upscale utilizando ESRGAN
def upscale_image(image_path, model_path):
    # Cargar la imagen
    image = Image.open(image_path).convert('RGB')
    
    # Convertir la imagen a tensor
    img_tensor = ToTensor()(image).unsqueeze(0).cuda()

    # Cargar el modelo preentrenado de ESRGAN
    model = torch.load(model_path)
    model = model['model']
    model.eval()
    model = model.cuda()

    # Realizar el upscale
    with torch.no_grad():
        output = model(img_tensor)

    # Convertir el tensor a imagen
    output_image = ToPILImage()(output.squeeze(0).cpu())
    return output_image

# Ruta de la imagen de entrada y del modelo de ESRGAN
image_path = 'ruta_a_tu_imagen.jpg'
model_path = 'ruta_al_modelo_ESRGAN.pth'

# Cargar la imagen
image = cv2.imread(image_path)

# Mejorar los detalles de la imagen
enhanced_image = enhance_details(image)

# Guardar la imagen mejorada temporalmente
temp_image_path = 'temp_image.jpg'
cv2.imwrite(temp_image_path, enhanced_image)

# Realizar el upscale utilizando ESRGAN
upscaled_image = upscale_image(temp_image_path, model_path)

# Guardar la imagen final
final_image_path = 'imagen_mejorada_y_upscaled.jpg'
upscaled_image.save(final_image_path)

print(f'Imagen mejorada y con upscale guardada en {final_image_path}')

